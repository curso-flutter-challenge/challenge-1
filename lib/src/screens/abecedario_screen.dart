import 'package:flutter/material.dart';

class AbecedarioScreen extends StatefulWidget {
  const AbecedarioScreen({Key? key}) : super(key: key);

  @override
  _AbecedarioScreenState createState() => _AbecedarioScreenState();
}

class _AbecedarioScreenState extends State<AbecedarioScreen> {
  int alfabeto = 65;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text('Challenge #1'),
        ),
        body: crearBody(),
        floatingActionButton: crearBotones());
  }

  Widget crearBody() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 10),
          Text(String.fromCharCode(alfabeto),
              style:
                  const TextStyle(fontSize: 150, fontWeight: FontWeight.bold)),
        ],
      ),
    );
  }

  Widget crearBotones() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        FloatingActionButton(
            onPressed: atras, child: const Icon(Icons.arrow_left)),
        const SizedBox(width: 10),
        FloatingActionButton(
            onPressed: adelante, child: const Icon(Icons.arrow_right)),
      ],
    );
  }

  void adelante() => setState(() {
        alfabeto++;
        if(alfabeto == 91){
          alfabeto = 65;
        }
      });

  void atras() => setState(() {
        alfabeto--;
        if(alfabeto == 64){
          alfabeto = 90;
        }
      });
}
